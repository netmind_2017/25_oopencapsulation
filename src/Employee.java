public class Employee {
	
	public int empid;
	public String empname;
	public String surname;
	
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		if (surname.indexOf(' ') >= 0)
			System.out.print("error Surname");
		else{			
			this.surname = surname;
		}
	}

	public void setEmpid(int empid){
		if (empid <=50)
			this.empid = empid;
		else
			System.out.print("error employee Id");
	}
	
	public void setEmpname(String name){
		
		if (name.indexOf(' ') >= 0) {
			System.out.println("error Name");
		}else{			
			this.empname = name;
		}	
	}

	public String GetName(){
		return this.empid + "  " + this.empname;
	}
	
}


